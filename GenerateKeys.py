from random import randrange, getrandbits
    
def is_prime(n, k=128):
    """ Test if a number is prime
        Args:
            n -- int -- the number to test
            k -- int -- the number of tests to do
        return True if n is prime
    """
    # Test if n is not even.
    # But care, 2 is prime !
    if n == 2 or n == 3:
        return True
    if n <= 1 or n % 2 == 0:
        return False
    # find r and s
    s = 0
    r = n - 1
    while r & 1 == 0:
        s += 1
        r //= 2
    # do k tests
    for _ in range(k):
        a = randrange(2, n - 1)
        x = pow(a, r, n)
        if x != 1 and x != n - 1:
            j = 1
            while j < s and x != n - 1:
                x = pow(x, 2, n)
                if x == 1:
                    return False
                j += 1
            if x != n - 1:
                return False
    return True

def generate_prime_candidate(length):
    """ Generate an odd integer randomly
        Args:
            length -- int -- the length of the number to generate, in bits
        return a integer
    """
    # generate random bits
    p = getrandbits(length)
    # apply a mask to set MSB and LSB to 1
    p |= (1 << length - 1) | 1
    return p
def generate_prime_number(length=3):
    """ Generate a prime
        Args:
            length -- int -- length of the prime to generate, in          bits
        return a prime
    """
    p = 4
    # keep generating while the primality test fail
    while not is_prime(p, 128):
        p = generate_prime_candidate(length)
    return p

def gcd(a,b): 
    if(b==0): 
        return a 
    else: 
        return gcd(b,a%b) 

def eulers_totient_function(n):
    n_primes = []
    for x in range(n):
        if(gcd(x,n) == 1):
            n_primes.append(x)
    
    return n_primes


def generate_keys():
    p = generate_prime_number()
    print("P: "+str(p))
    q = generate_prime_number()
    if(q == p):
        while(q == p):
            q = generate_prime_number()
    print("Q: "+str(q))

    n = p*q
    print("N: "+str(n))
    primes = eulers_totient_function(n)
    phi_n = len(primes)
    print("Phi N: "+str(phi_n))

    if phi_n == ((p-1)*(q-1)):
        print("in")
        e = randrange(0, n, 1)
        
        while not gcd(e,phi_n) == 1:
            e = randrange(0, n, 1)

        d = (phi_n+1) / e
        if(d < phi_n):
            print("E : " +str(e))
            print("D : " +str(d))
            pub = [e,n]

    return d, pub

def get_keys():
    private_key, public_key = generate_keys()
    print("PRIVATE "+str(private_key))
    print("PUBLIC : e: "+ str(public_key[0])+" n: "+str(public_key[1]))


def encrypt(plaintext, public_e, public_n):
    ciphertext = []
    i = len(plaintext)
    for x in range(i):
        numeric = numeriate(plaintext[x])
        power = numeric ** public_e
        cipher = power%public_n
        ciphertext.append(cipher)

    return ciphertext

def decrypt(ciphertext, private_d, public_n):
    plaintext = []
    i = len(ciphertext)
    for x in range(i):
        power = ciphertext[x] ** private_d
        numeric = power%public_n
        plain = charify(numeric)
        plaintext.append(plain)
    return plaintext

def numeriate(letter):
    cipher = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','y','z']
    for x in range(len(cipher)):
        if(cipher[x] == letter):
            return x+1

def charify(number):
    cipher = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','y','z']
    for x in range(len(cipher)):
        if(x == number):
            return cipher[x-1]

plain = raw_input("Enter plaintext: ")
private_key, public_key = generate_keys()
cipher = encrypt(plain, public_key[0], public_key[1])
print("Cipher : ")
for x in range(len(cipher)):
    print(cipher[x])
plain = decrypt(cipher, private_key, public_key[1])
print("Decrypted :")
for x in range(len(plain)):
    print(plain[x])

