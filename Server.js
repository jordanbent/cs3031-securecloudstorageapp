const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();

router.get('/',function(req,res){
  res.sendFile(path.join(__dirname+'/Webpage.html'));
});

router.get('/Your_Groups',function(req,res){
  res.sendFile(path.join(__dirname+'/Your_Groups.html'));
});

router.get('/Create_Group',function(req,res){
  res.sendFile(path.join(__dirname+'/Create_Group.html'));
});

//add the router
app.use('/', router);
app.listen(process.env.port || 3000);

console.log('Running at Port 3000');