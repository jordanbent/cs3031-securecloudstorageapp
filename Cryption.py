
from cryptography.fernet import Fernet

def generate_key():
    key = Fernet.generate_key()
    return key

def encrypt(key, filename):
    cipher_suite = Fernet(key)
    with open(filename, "rb") as afile:
        text = afile.read()
    print(text)
    cipher_text = cipher_suite.encrypt(text)
    with open("{0}".format(filename), "wb") as bfile:
        bfile.write(cipher_text)
    with open("{0}".format(filename), "rb") as afile:
        ctext = afile.read()
    print(ctext)

def decrypt(key, filename):
    cipher_suite = Fernet(key)
    with open(filename, "rb") as afile:
        cipher_text = afile.read()
    print(cipher_text)
    plain_text = cipher_suite.decrypt(cipher_text)
    with open("{0}".format(filename), "wb") as dfile:
        dfile.write(plain_text)
    with open("{0}".format(filename), "rb") as afile:
        text = afile.read()
    print(text)

key = generate_key()
encrypt(key, "plain.txt")
decrypt(key, "plain.txt")